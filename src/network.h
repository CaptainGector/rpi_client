/*
 * network.h
 *
 *  Created on: Nov 13, 2020
 *      Author: gector
 */

#ifndef SRC_NETWORK_H_
#define SRC_NETWORK_H_

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>

	// Network code was learned/copied from
	// https://beej.us/guide/bgnet/
	// (Mostly learned, my structuring is a bit different)

// Function prototypes
int attachTo(char *ip, char *port);
int waitReply(char *buffer, int len, int timeout);
int getReply(char *buffer, int len);
void sendCommand(char *command, int len);
void closeUp();

int mySock;

#endif /* SRC_NETWORK_H_ */
