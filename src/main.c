#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <iobb.h>

// My code
#include "network.h"
#include "msgQueue.h"

// Buffers for ip and port
char ipAddr[20] = "192.168.1.148";
char port[10] = "51717";

// Delete newline characters in a string (and carriage returns.)
void removeNewlines(char *str) {
	int x = 0;
	while (str[x] != 0) {
		if (str[x] == 0xA || str[x] == 0xD) {
			str[x] = 0;
		}
		x++;
	}
}

// Send Command
void sendCommandConst(const char *str) {
	char commandBuf[32];
	strcpy(commandBuf, str);
	sendCommand(commandBuf, strlen(commandBuf));
}

// Send a command and get a reply
int sendAndGet(const char *str, char *buf, int len) {
	strcpy(buf, str);
	sendCommand(buf, len);
	if (waitReply(buf, len, 20)){
		removeNewlines(buf);
		return 0;
	}
	return -1;
}

// An overcomplicated approach originating from nearly 24hrs without sleep.
// Takes a pointer to a string
// Takes the length of the string
// Returns an integer derived from the string fed to it.
int getIntFromString(char *str, int len) {
	int countLast;
	char *ptr;
	char dest[len];
	memset(dest, 0, sizeof(dest));

	// Find the first number
	int count = 0;
	while (str[count] != 0) {
		if ((str[count] >= 48 && str[count] <= 57) || str[count] == 45) {
			break;
		}
		count++;
	}

	// Count now contains the position of the first number
	if (count > 0) {
		// ... now find the last number...
		countLast = count;
		while (str[countLast] != 0) {
			if ((str[countLast] >= 48 && str[countLast] <= 57) || str[countLast] == 45) {
				countLast++;
				continue;
			}
			break;
		}

		if ((countLast-count) < len) {
			// Success!
			strncpy(dest, str+count, countLast-count);
			return (int)strtol(dest, &ptr, 10);
		} else {
			return 0;
		}
	}
	return 0;
}



int main(int argc, char *argv[]) {

	if (createMsgQueues(1) < 0) {
		printf("Error creating message queue!\n");
	}

	int runTime = 3600*5; // 5hrs run time by default.
	//if (argc >= 2) {
	//	runTime = strtol(argv[1], NULL, 10);
	//}

	//char command[32];
	char buffer[100];

	// read in arguments if there are any
	if (argc == 2) {
		// IP only
		strcpy(ipAddr, argv[1]);
	} else if (argc == 3) {
		// IP and Port
		strcpy(ipAddr, argv[1]);
		strcpy(port, argv[2]);

	}

	// ------- TEST CODE ----------

	// Create a connection to the server
	if (attachTo(ipAddr, port) != 0) {
		closeUp();
	}

	if (!(argc > 1)) {
		// Get a reply & print
		waitReply(buffer, sizeof(buffer), 1);
		removeNewlines(buffer);
		printf("Reply: '%s'\n", buffer);

		sendAndGet("RUNNING TESTS...", buffer, sizeof(buffer));
		printf("Reply: '%s'\n", buffer);
		printf("Value: %d\n", getIntFromString(buffer, strlen(buffer)));

		sendAndGet("ACCEL READ X", buffer, sizeof(buffer));
		printf("Reply: '%s'\n", buffer);
		printf("Value: %d\n", getIntFromString(buffer, strlen(buffer)));

		sendAndGet("ACCEL READ Y", buffer, sizeof(buffer));
		printf("Reply: '%s'\n", buffer);
		printf("Value: %d\n", getIntFromString(buffer, strlen(buffer)));

		sendAndGet("ACCEL READ Z", buffer, sizeof(buffer));
		printf("Reply: '%s'\n", buffer);
		printf("Value: %d\n", getIntFromString(buffer, strlen(buffer)));

		sendAndGet("GYRO READ X", buffer, sizeof(buffer));
		printf("Reply: '%s'\n", buffer);
		printf("Value: %d\n", getIntFromString(buffer, strlen(buffer)));

		sendAndGet("GYRO READ Y", buffer, sizeof(buffer));
		printf("Reply: '%s'\n", buffer);
		printf("Value: %d\n", getIntFromString(buffer, strlen(buffer)));

		sendAndGet("GYRO READ Z", buffer, sizeof(buffer));
		printf("Reply: '%s'\n", buffer);
		printf("Value: %d\n", getIntFromString(buffer, strlen(buffer)));

		sendAndGet("TEMP READ", buffer, sizeof(buffer));
		printf("Reply: '%s'\n", buffer);
		printf("Value: %d\n", getIntFromString(buffer, strlen(buffer)));

		sendAndGet("STEPPER SET SPEED 0", buffer, sizeof(buffer));
		printf("Reply: '%s'\n", buffer);

		sendAndGet("TESTS DONE. GOODBYE!", buffer, sizeof(buffer));
		printf("Reply: '%s'\n", buffer);
		printf("Value: %d\n", getIntFromString(buffer, strlen(buffer)));
	}


	// Send a message to the queue.
	printf("Running message Queue\n");
	char msgBuf[MAX_MSG_SIZE] = {};
	strcpy(msgBuf, "Hello GUI program!");
	sendToOther(msgBuf, MAX_MSG_SIZE);
	sleep(1);

	// Do message queue commands
	struct timeval oldTime;
	struct timeval nowTime;
	gettimeofday(&oldTime, NULL);
	gettimeofday(&nowTime, NULL);
	while ((nowTime.tv_sec - oldTime.tv_sec) < runTime) {
		gettimeofday(&nowTime, NULL);

		if (readFromOther(msgBuf, MAX_MSG_SIZE) > 0)  {
			printf("Received Message: %s\n", msgBuf);
			sendAndGet(msgBuf, buffer, sizeof(buffer));
			printf("Reply: '%s'\n", buffer);
			strcpy(msgBuf, buffer);
			sendToOther(msgBuf, MAX_MSG_SIZE);
		} else {
			memset(msgBuf, 0, MAX_MSG_SIZE);
		}

		usleep(500000);
	}

	// Close our files.
	closeQueues();
	closeUp();

	return 0; // Success!
}
