/*
 * network.c
 *
 *  Created on: Nov 13, 2020
 *      Author: Zachary Whitlock
 */

#include "network.h"

/* (Design psuedo code \/) --------------------------------------
 *
 * First, create a connection to the server
 *
 * Second, send a command to the server
 *
 * Third, allow non-blocking recieving from the server
 *
 * Fourth, close up shop.
 */

void closeUp() {
	close(mySock);
}

void sendCommand(char *command, int len) {
	int sentBytes;
	sentBytes = send(mySock, command, len, 0);
	if (sentBytes == -1) {
		printf("FAILED to send any data.\n");
	} else if(sentBytes != len) {
		// Keep trying to send data.
		while (sentBytes < len) {
			sentBytes += send(mySock, command, len, 0);
			printf("SENDING AGAIN\n");
		}
	} else {
		printf("Sent: '%s'\n", command);
	}

}

// Will fill a buffer with anything received from server
// Returns 0 if no data
int getReply(char *buffer, int len) {
	if (recv(mySock, buffer, len, 0))
		return 1;

	return 0;
}


// Will wait for a reply from the server.
// timeout is in seconds
// Returns -1 if timeout
int waitReply(char *buffer, int len, int timeout) {
	int recievedBytes;
	struct timeval oldTime;
	struct timeval nowTime;

	memset(buffer, 0, len);

	gettimeofday(&oldTime, NULL);
	gettimeofday(&nowTime, NULL);
	while ((nowTime.tv_sec - oldTime.tv_sec) < timeout) {
		gettimeofday(&nowTime, NULL);
		recievedBytes = recv(mySock, buffer, len, 0);
		if (recievedBytes > 0) {
			return recievedBytes;
		}
		usleep(100);
	}
	return -1;
}


// Create a socket and attach to a server.
int attachTo(char *ip, char *port) {
	struct addrinfo hints, *res;
	int yes = 1;

	// Setup struct
	memset(&hints,0,sizeof hints);
	hints.ai_family = AF_UNSPEC; // Unspecified
	hints.ai_socktype = SOCK_STREAM; // TCP.

	// get info about the server
	getaddrinfo(ip, port, &hints, &res);

	// get a new sock!(et)
	mySock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (mySock == -1) {
		printf("Failed to create socket.\n");
		return -1;
	}

	// Allow re-using the socket for easy and fast debugging.
	setsockopt(mySock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

	// Connect to server.
	if (connect(mySock, res->ai_addr, res->ai_addrlen) == -1){
		printf("Failed to connect with socket.\n");
		return -1;
	}

	// Disable blocking
	fcntl(mySock, F_SETFL, O_NONBLOCK);

	// Send a message!
	sendCommand("HELLO SERVER", strlen("HELLO SERVER"));

	return 0;
}

